const {swaggerBasePath, swaggerHost, swaggerSchemes} = require('./configs');

const swaggerScheme = {
  'swagger': '2.0',
  'info': {
    'version': '',
    'title': 'Cats API Photo',
  },
  'host': swaggerHost,
  'basePath': swaggerBasePath,
  'schemes': swaggerSchemes,
  'paths': {
    '/cats/{catId}/upload': {
      'post': {
        'tags': [
          'Добавить',
        ],
        'summary': 'Загрузить фото для кота',
        'description': 'Добавление фото для кота по ID',
        'parameters': [
          {
            'name': 'catId',
            'in': 'path',
            'description': 'Уникальный ID кота',
            'required': true,
            'type': 'number',
          },
          {'name': 'file',
            'in': 'formData',
            'description': 'Картинка для загрузки',
            'required': true,
            'type': 'file'},
        ],
        'responses': {
          '200': {
            'description': 'Имя сохраненной картинки',
            'schema': {
              'type': 'object',
              'properties': {
                'fileUrl': {
                  'type': 'string',
                },
              },
            },
          },
          '400': {'description': 'Неверный формат ID'},
        },
      },
    },
    '/cats/{catId}/photos': {
      'get': {
        'tags': [
          'Получить',
        ],
        'summary': 'Получить имена сохраненных картинок для кота по ID',
        'description': 'Получение имен сохраненных картинок для кота по ID',
        'parameters': [
          {
            'name': 'catId',
            'in': 'path',
            'description': 'Уникальный ID кота',
            'required': true,
            'type': 'number',
          },
        ],
        'responses': {
          '200': {
            'description': 'Имя сохраненной картинки',
            'schema': {
              'type': 'object',
              'properties': {
                'images': {
                  'type': 'array',
                  'items': {
                    'type': 'string',
                  },
                },
              },
            },
          },
          '400': {'description': 'Неверный формат ID'},
        },
      },
    }}};
module.exports = {
  swaggerScheme,
};
