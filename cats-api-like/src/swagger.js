const {swaggerBasePath, swaggerHost, swaggerSchemes} = require('./configs');

const swaggerScheme = {
  'swagger': '2.0',
  'info': {
    'version': '',
    'title': 'Cats API Like',
  },
  'host': swaggerHost,
  'basePath': swaggerBasePath,
  'schemes': swaggerSchemes,
  'paths': {
    '/cats/{catId}/likes': {
      'post': {
        'tags': [
          'Лайк/Дизлайк',
        ],
        'summary': 'Поставить/убрать лайк/дизлайк коту',
        'description': 'Поставить/убрать лайк/дизлайк коту по ID',
        'parameters': [
          {
            'name': 'catId',
            'in': 'path',
            'description': 'Уникальный ID кота',
            'required': true,
            'type': 'number',
          },
          {
            'name': 'json',
            'in': 'body',
            'description': 'Объект запроса для необходимого действия',
            'schema': {
              'type': 'object',
              'properties': {
                'like': {
                  'description': 'Поставить лайк - true, убрать лайк - false',
                  'type': 'boolean',
                  'example': true,
                },
                'dislike': {
                  'description': 'Поставить дизлайк - true, убрать дизлайк - false',
                  'type': 'boolean',
                  'example': false,
                },
              },
            },
            'required': true,
          },
        ],
        'responses': {
          '200': {
            'description': 'Кот, для которого было применено действие',
            'schema': {
              '$ref': '#/definitions/Cat',
            },
          },
          '400': {'description': 'Неверный формат ID'},
        },
      },
    },
    '/cats/{catId}/like': {
      'post': {
        'tags': [
          'Лайк',
        ],
        'deprecated': true,
        'summary': 'Поставить лайк коту',
        'description': 'Поставить лайк коту',
        'parameters': [
          {
            'name': 'catId',
            'in': 'path',
            'description': 'Уникальный ID кота которому необходимо поставить лайк',
            'required': true,
            'type': 'number',
          },
        ],
        'responses': {
          '200': {
            'description': 'Кот, для которого было применено действие',
            'schema': {
              '$ref': '#/definitions/Cat',
            },
          },
        },
      },
      'delete': {
        'tags': [
          'Лайк',
        ],
        'deprecated': true,
        'summary': 'Убрать лайк у кота',
        'description': 'Убрать лайк у кота',
        'parameters': [
          {
            'name': 'catId',
            'in': 'path',
            'description': 'Уникальный ID кота у которого необходимо убрать лайк',
            'required': true,
            'type': 'number',
          },
        ],
        'responses': {
          '200': {
            'description': 'Кот, для которого было применено действие',
            'schema': {
              '$ref': '#/definitions/Cat',
            },
          },
        },
      },
    },
    '/cats/{catId}/dislike': {
      'post': {
        'tags': [
          'Дизлайк',
        ],
        'deprecated': true,
        'summary': 'Добавить дизлайк коту',
        'description': 'Добавление дизлайка коту',
        'parameters': [
          {
            'name': 'catId',
            'in': 'path',
            'description': 'Уникальный ID кота для добавления дизлайка',
            'required': true,
            'type': 'number',
          },
        ],
        'responses': {
          '200': {
            'description': 'Кот, для которого было применено действие',
            'schema': {
              '$ref': '#/definitions/Cat',
            },
          },
        },
      },
      'delete': {
        'tags': [
          'Дизлайк',
        ],
        'deprecated': true,
        'summary': 'Убрать дизлайк у кота',
        'description': 'Убрать дизлайк у кота',
        'parameters': [
          {
            'name': 'catId',
            'in': 'path',
            'description': 'Уникальный ID кота у которого необходимо убрать дизлайка',
            'required': true,
            'type': 'number',
          },
        ],
        'responses': {
          '200': {
            'description': 'Кот, для которого было применено действие',
            'schema': {
              '$ref': '#/definitions/Cat',
            },
          },
        },
      },
    },
    '/cats/likes-rating': {
      'get': {
        'tags': [
          'Лайк',
        ],
        'summary': 'Получить список "ТОП-10 лайков"',
        'description': 'Получение списка "ТОП-10 лайков"',
        'produces': [
          'application/json',
        ],
        'responses': {
          '200': {
            'description': 'Список котов с количеством лайков',
            'schema': {
              'type': 'array',
              'items': {
                'type': 'object',
                'properties': {
                  'name': {
                    'type': 'string',
                    'example': 'Кот',
                  },
                  'likes': {
                    'type': 'number',
                    'example': 12,
                  },
                  'id': {
                    'type': 'number',
                    'example': 1,
                  },
                },
              },
            },
          },
        },
      },
    },
    '/cats/dislikes-rating': {
      'get': {
        'tags': [
          'Дизлайк',
        ],
        'description': 'Получить список "ТОП-10 дизлайков"',
        'summary': 'Получение списка "ТОП-10 дизлайков"',
        'produces': [
          'application/json',
        ],
        'responses': {
          '200': {
            'description': 'Список котов с количеством дизлайков',
            'schema': {
              'type': 'array',
              'items': {
                'type': 'object',
                'properties': {
                  'name': {
                    'type': 'string',
                    'example': 'Кот',
                  },
                  'dislikes': {
                    'type': 'number',
                    'example': 3,
                  },
                  'id': {
                    'type': 'number',
                    'example': 1,
                  },
                },
              },
            },
          },
        },
      },
    },
    '/cats/rating': {
      'get': {
        'tags': [
          'Рейтинг',
        ],
        'summary': 'Получение списка "ТОП-10 лайков и дизлайков"',
        'produces': [
          'application/json',
        ],
        'responses': {
          '200': {
            'description': 'Список котов с количеством лайков и дизлайков',
            'schema': {
              'type': 'object',
              'properties': {
                'like': {
                  'type': 'array',
                  'items': {
                    'type': 'object',
                    'properties': {
                      'id': {
                        'type': 'string',
                        'example': 1,
                      },
                      'name': {
                        'type': 'string',
                        'example': 'Кот',
                      },
                      'likes': {
                        'type': 'number',
                        'example': 12,
                      },
                    },
                  },
                },
                'dislike': {
                  'type': 'array',
                  'items': {
                    'type': 'object',
                    'properties': {
                      'id': {
                        'type': 'string',
                        'example': 1,
                      },
                      'name': {
                        'type': 'string',
                        'example': 'Кот',
                      },
                      'dislikes': {
                        'type': 'number',
                        'example': 4,
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
  },
  'definitions': {
    'Cat': {
      'type': 'object',
      'properties': {
        'id': {'type': 'integer', 'format': 'int32',
          'example': 12,
        },
        'name': {'type': 'string',
          'example': 'кот',
        },
        'description': {'type': 'string', 'example': 'Описание'},
        'tags': {'type': 'string',
          'example': null,
        },
        'gender': {'$ref': '#/definitions/GenderEnum'},
        'likes': {'type': 'integer', 'format': 'int32',
          'example': 12,
        },
        'dislikes': {'type': 'integer', 'format': 'int32',
          'example': 5,
        },
      },
    },
    'GenderEnum': {
      'type': 'string',
      'description': 'Пол кота. Может принимать значения: male, female, unisex',
      'enum': ['male', 'female', 'unisex'],
      'example': 'unisex',
    },
  }};
module.exports = {
  swaggerScheme,
};
