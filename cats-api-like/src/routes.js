const {sendResponse, sendError} = require('./helper-response');
const express = require('express');
const swaggerUi = require('swagger-ui-express');
const {swaggerScheme} = require('./swagger');
const catsStorage = require('./storage');
const {pool} = require('./storage');
const cors = require('cors');
const {logger} = require('./logger');
const {serviceVersion} = require('./configs');
const bodyParser = require('body-parser');

const app = express();

function isEmpty(value) {
  return value == null || value.length === 0;
}

/**
 * Проверка, что id из заспроса - целое и положительное число
 * @param {string} id - пришедший в запросе id
 */
function isValidId(id) {
  // Используем Number(...) т.к. parseFloat/parseInt могут обработать валидный префикс в невалидной строке
  // Например parseFloat('1абс') = 1
  return Number.isInteger(parseFloat(id));
}

app.use((err, req, res, next) => {
  logger.error(err.toString());
  next();
});
app.use(bodyParser.json());
app.use(cors());

app.get('/status', async (req, res) => {
  try {
    const bdVersion = (await pool.query('SELECT version()')).rows;
    return sendResponse(req, res, {
      version: serviceVersion,
      connectionToDB: {
        status: true,
        dbInfo: bdVersion,
      },
    });
  } catch (err) {
    return sendError(req, res, {code: 500, message: {
      version: serviceVersion,
      connectionToDB: {
        status: false,
        dbInfo: err.toString(),
      },
    }});
  }
});

app.post('/cats/:catId/like', async (req, res) => {
  const {catId} = req.params;

  if (isEmpty(catId)) {
    return sendError(req, res, {code: 400, message: 'Некорректный параметр ID'});
  }
  try {
    return sendResponse(req, res, await catsStorage.plusLike(catId));
  } catch (err) {
    return sendError(req, res, err);
  }
});

app.post('/cats/:catId/likes', async (req, res) => {
  const {catId} = req.params;
  const {like, dislike} = req.body;

  if (isEmpty(catId)) {
    return sendError(req, res, {code: 400, message: 'Некорректный параметр ID'});
  }

  if (!isValidId(catId)) {
    return sendError(req, res, {
      code: 400,
      message: 'Буквы в ID не принимаются',
    });
  }

  if (like === undefined && dislike === undefined) {
    return sendError(req, res, {code: 400, message: 'Ошибка json'});
  }

  if (like && dislike) {
    return sendError(req, res, {code: 400, message: 'Нельзя установить одновременно лайк и дизлайк'});
  }

  let _response = null;
  try {
    if (like === true) {
      _response = await catsStorage.plusLike(catId);
    } else if (like === false) {
      _response = await catsStorage.minusLike(catId);
    }
  } catch (err) {
    return sendError(req, res, err);
  }

  try {
    if (dislike === true) {
      _response = await catsStorage.plusDislike(catId);
    } else if (dislike === false) {
      _response = await catsStorage.minusDislike(catId);
    }
  } catch (err) {
    return sendError(req, res, err);
  }
  sendResponse(req, res, _response);
});

app.delete('/cats/:catId/like', async (req, res)=>{
  const {catId} = req.params;
  if (isEmpty(catId)) {
    return sendError(req, res, {code: 400, message: 'Некорректный параметр ID'});
  }
  try {
    return sendResponse(req, res, await catsStorage.minusLike(catId));
  } catch (err) {
    return sendError(req, res, err);
  }
});

app.post('/cats/:catId/dislike', async (req, res) => {
  const {catId} = req.params;
  if (isEmpty(catId)) {
    return sendError(req, res, {code: 400, message: 'Некорректный параметр ID'});
  }
  try {
    return sendResponse(req, res, (await catsStorage.plusDislike(catId)));
  } catch (err) {
    return sendError(req, res, err);
  }
});

app.delete('/cats/:catId/dislike', async (req, res) => {
  const {catId} = req.params;
  if (isEmpty(catId)) {
    return sendError(req, res, {code: 400, message: 'Некорректный параметр ID'});
  }
  try {
    return sendResponse(req, res, await catsStorage.minusDislike(catId));
  } catch (err) {
    return sendError(req, res, err);
  }
});

app.get('/cats/likes-rating', async (req, res) => {
  try {
    return sendResponse(req, res, await catsStorage.getLikesRating());
  } catch (err) {
    return sendError(req, res, 'Ошибка получения рейтинга дизлайков', err.stack || err.message);
  }
});

app.get('/cats/dislikes-rating', async (req, res) => {
  try {
    return sendResponse(req, res, await catsStorage.getDislikesRating());
  } catch (err) {
    return sendError(req, res, 'Ошибка получения рейтинга дизлайков', err.stack || err.message);
  }
});

app.get('/cats/rating', async (req, res) => {
  try {
    return sendResponse(req, res,
        {likes: await catsStorage.getLikesRating(), dislikes: await catsStorage.getDislikesRating()});
  } catch (err) {
    return sendError(req, res, err);
  }
},
);

app.use('/api-docs-ui', swaggerUi.serve, swaggerUi.setup(swaggerScheme, {swaggerOptions: {
  defaultModelsExpandDepth: -1},
}));

app.get('*', (req, res)=>{
  return sendError(req, res, {code: 404, message: 'Страница не найдена'});
});

app.post('*', (req, res)=>{
  return sendError(req, res, {code: 404, message: 'Страница не найдена'});
});

app.delete('*', (req, res)=>{
  return sendError(req, res, {code: 404, message: 'Страница не найдена'});
});

app.put('*', (req, res)=>{
  return sendError(req, res, {code: 404, message: 'Страница не найдена'});
});

module.exports=app;
