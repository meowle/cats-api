const {
  groupNamesAndSort,
  checkValidCatName,
  checkValidAddCat,
  groupNamesAndSortByLetter
} = require('./helpers');
const catsStorage = require('./storage');

async function addCats(cats) {
  const catsResponse = {cats: []};
  for (let i = 0; i < cats.length; i++) {
    cats[i].name = cats[i].name.toLowerCase();
    const checkError = await checkValidAddCat(cats[i], 'add');
    if (checkError) {
      catsResponse.cats.push({
        cat: cats[i],
        errorDescription: checkError,
      });
    } else {
      try {
        cats[i].name = cats[i].name[0].toUpperCase() + cats[i].name.slice(1);
        catsResponse.cats.push(await catsStorage.addCats(cats[i]));
      } catch (err) {
        catsResponse.cats.push({
          cat: cats[i],
          errorDescription:
            err.code === '23505' ? 'Такое имя уже есть!' : err.message,
        });
      }
    }
  }
  return catsResponse;
}

async function allCats(gender, order) {
  const reverseSort = (order || 'asc').toLowerCase() === 'desc';
  const allCats = await catsStorage.allCats(gender);
  return groupNamesAndSort(allCats, reverseSort);
}

async function allByLetter(limit, order, gender) {
  const sort = order === undefined ? 'asc' : order.toLowerCase();
  const allCats = await catsStorage.allByLetter(limit, sort, gender);
  return groupNamesAndSortByLetter(allCats, false);
}

async function findCatsByParams(searchParams) {
  const foundCats = await catsStorage.findCatsByParams(searchParams);
  const reverseSort = (searchParams.order || 'asc').toLowerCase() === 'desc';
  const checkError = await checkValidCatName(searchParams.name, 'search');
  if (checkError) {
    throw {code: 400, message: checkError};
  }
  return groupNamesAndSort(foundCats, reverseSort);
}

module.exports = {
  addCats,
  allCats,
  allByLetter,
  findCatsByParams,
};
